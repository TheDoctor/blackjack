import os
from time import sleep
from random import randint

class Main():

    # Player
    coins = 20
    points = 0
    bet = 1
    cards = []

    # Dealer
    d_points = 0
    d_cards = []

    # Game
    #pos_cards = ['A', 'A', 'A', 'A', 'B', 'B', 'B', 'B', 'D', 'D', 'D', 'D', 'K', 'K', 'K', 'K',
    #             '10', '10', '10', '10', '9', '9', '9', '9', '8', '8', '8', '8', '7', '7', '7', '7', '6', '6', '6', '6']
    pos_cards = ['A', 'A', 'A', 'A', 'B', 'B', 'B', 'B', 'D', 'D', 'D', 'D', 'K', 'K', 'K', 'K',
                 '10', '10', '10', '10', '9', '9', '9', '9', '8', '8', '8', '8', '7', '7', '7', '7', '6', '6', '6', '6',
                 '5', '5', '5', '5', '4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2']
    rem_cards = pos_cards
    card_values = {'B': 10, 'D': 10, 'K': 10, '10': 10, '9': 9, '8': 8, '7': 7,
                    '6': 6,'5': 5, '4': 4, '3': 3, '2': 2}

    def __init__(self):
        self.main_menu()

    def main_menu(self):
        while True:
            self.clear()
            print("-=-=-= Thogs' Blackjack =-=-=-")
            print("1 -> Start Blackjack")
            print()
            print("0 -> Exit")

            cmd = input("~> ")

            if cmd == "1":
                self.blackjack()

            elif cmd == "0":
                exit(0)

            else:
                print("[!] Invalid input!")
                sleep(1.5)

    def blackjack(self):
        self.fresh_start()
        while self.coins > 0:
            self.play_round()

        self.clear()
        print("-~-~-~ GAME OVER ~-~-~-")
        print("You've ran out of coins!")
        print()
        input("Press ENTER to go back to main menu...")

    def play_round(self):
        self.points = 0
        self.bet = 1
        self.cards = []
        self.get_card()

        b_get_cards = True
        while b_get_cards:
            self.clear()
            print("~:~:~: Blackjack :~:~:~")
            print()
            self.print_cards(self.cards)
            print()
            print("Coins: " + str(self.coins), end="\t")
            print("Points: " + str(self.points), end="\t")
            print("Bet: " + str(self.bet))
            print()

            if self.points < 21:
                print("1 -> Take another card")
                print("2 -> Play against the dealer")
                print("3 -> Raise bet")
                print()

                cmd = input("~> ")

                if cmd == "1":
                    self.get_card()

                elif cmd == "2":
                    b_get_cards = False

                elif cmd == "3":
                    print()
                    try:
                        new_bet = int(input("Raise by ~> "))

                        if self.coins - new_bet <= 0:
                            raise Exception()

                        else:
                            self.bet += new_bet
                            self.get_card()

                    except:
                        print("\n[!] Invalid input!")
                        sleep(1.5)

                else:
                    print("[!] Invalid input!")
                    sleep(1.5)

            elif self.points == 21:
                print("[*] You've got a Blackjack!")
                print()
                input("Press ENTER to continue...")

                self.coins += self.bet

                return

            else:
                print("[-] You've lost.")
                print()
                input("Press ENTER to continue...")

                self.coins -= self.bet

                return

        # Play against the dealer
        self.d_points = 0
        self.d_cards = []

        while self.d_points < 18:
            self.get_card(True)

        self.clear()
        print("-<>-<>-< Dealer >-<>-<>-")
        print()
        self.print_cards(self.d_cards)
        print()
        print("Points: " + str(self.d_points))
        print()

        if self.points > self.d_points:
            print("[*] You've won with " + str(self.points) + " points!")

            self.coins += self.bet

        elif self.d_points > 21:
            print("[*] You've won - the dealer became too greedy!")

            self.coins += self.bet

        else:
            print("[-] You've lost with " + str(self.points) + " points.")

            self.coins -= self.bet

        print()
        input("Press ENTER to continue...")

    def print_cards(self, cards):
        print("Cards:", end="")
        for card in cards:
            print(" [" + card + "]", end="")
        print()

    def fresh_start(self):
        self.coins = 20
        self.points = 0
        self.rem_cards = self.pos_cards

    def get_card(self, b_dealer=False):
        if len(self.rem_cards) > 0:
            card = self.rem_cards[randint(0, len(self.rem_cards) - 1)]

            # Dealer
            if b_dealer:
                self.d_cards.append(card)

                if card == 'A':
                    self.d_points += 1
                    if self.d_points + 10 <= 21:
                        self.d_points += 10

                else:
                    self.d_points += self.card_values[card]

            # Player
            else:
                self.cards.append(card)

                if card == 'A':
                    self.points += 1
                    if self.points + 10 <= 21:
                        self.points += 10

                else:
                    self.points += self.card_values[card]

        else:
            self.rem_cards = self.pos_cards
            self.get_card()

    def clear(self):
        os.system('cls' if os.name == 'nt' else 'clear')


if __name__ == "__main__":
    Main()